package grpcclient

import (
	"fmt"
	"sync"

	configs "gitlab.com/auth_ban/api_gateway/internal/pkg/config"
	auth "gitlab.com/auth_ban/api_gateway/internal/genproto/auth_service"
	"google.golang.org/grpc"
)

var cfg = configs.Config()
var (
	onceAuthService     sync.Once
	instanceAuthService     auth.AuthServiceClient
)

// UserService ...
func AuthService() auth.AuthServiceClient {
	onceAuthService.Do(func() {
		connAuth, err := grpc.Dial(
			fmt.Sprintf("%s:%d", cfg.AuthServiceHost, cfg.AuthServicePort),
			grpc.WithInsecure())
		if err != nil {
			panic(fmt.Errorf("auth service dial host: %s port:%d err: %s",
				cfg.AuthServiceHost, cfg.AuthServicePort, err))
		}

		instanceAuthService = auth.NewAuthServiceClient(connAuth)
	})

	return instanceAuthService
}