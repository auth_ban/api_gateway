package middleware

import (
	"gitlab.com/auth_ban/api_gateway/internal/controller/errors"
	"fmt"
	"log"
	"net/http"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"gitlab.com/auth_ban/api_gateway/internal/pkg/jwt"
	configs "gitlab.com/auth_ban/api_gateway/internal/pkg/config"
)

var config = configs.Config()

func Authorizer() gin.HandlerFunc {
	return func(c *gin.Context) {
		accessToken := c.GetHeader("Authorization")
		enforcer, err := casbin.NewEnforcer(config.CasbinConfigPath, config.MiddlewareRolesPath)
		if err != nil {
			log.Fatal("enforcer not initialized, ", err)
			return
		}

		claims, err := extractClaims(accessToken, []byte(config.JWTSecretKey))
		if err != nil {
			c.JSON(http.StatusBadRequest, map[string]string{
				"error": err.Error(),
			})
			c.Abort()
			return
		}

		role := claims["role"]
		fmt.Println(role, c.Request.URL.String(), c.Request.Method)
		ok, err := enforcer.Enforce(role, c.Request.URL.String(), c.Request.Method)
		if err != nil {
			log.Println("could not enforce:", err)
			c.Abort()
			return
		}

		if !ok {
			c.JSON(http.StatusForbidden, map[string]string{
				"error": "user not allowed, there is a problem with authorization",
			})
			c.Abort()
			return
		}

		c.Next()
	}
}

type JWTRoleAuthorizer struct {
	enforcer   *casbin.Enforcer
	SigningKey []byte
	//	logger     logger.Logger
}

// NewJWTRoleAuthorizer creates and returns new Role Authorizer
func NewJWTRoleAuthorizer(cfg *configs.Configuration) (*JWTRoleAuthorizer, error) {
	enforcer, err := casbin.NewEnforcer(cfg.CasbinConfigPath, cfg.MiddlewareRolesPath)
	if err != nil {
		log.Fatal("could not initialize new enforcer:", err.Error())
		return nil, err
	}

	return &JWTRoleAuthorizer{
		enforcer:   enforcer,
		SigningKey: []byte(cfg.JWTSecretKey),
	}, nil
}

// NewAuthorizer returns middleware function to be used by fiber app for authorization
func NewAuthorizer(jwtra *JWTRoleAuthorizer) gin.HandlerFunc {
	return func(c *gin.Context) {
		accessToken := c.GetHeader("Authorization")

		claims, err := jwt.ExtractClaims(accessToken, jwtra.SigningKey)
		if err != nil {
			log.Println("could not extract claims:", err)
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		role := claims["role"]

		ok, err := jwtra.enforcer.Enforce(role, c.FullPath(), c.Request.Method)
		if err != nil {
			log.Println("could not enforce:", err)
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}

		if !ok {
			c.AbortWithStatusJSON(http.StatusForbidden, errors.ErrorResponse{
				Code:    http.StatusForbidden,
				Message: errors.NotEnoughRights,
			})
			return
		}
	
		c.Next()
	}
}
