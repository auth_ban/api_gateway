package models

type LoginRequest struct {
	PhoneNumber string `json:"phone_number"`
	Password    string `json:"password"`
}

type LoginResponse struct {
	UserID    string `json:"user_id"`
	Authorize string `json:"authorize"`
}

type SuccessMessage struct {
	Success bool `json:"success"`
}

type CreateUserRequest struct {
	FullName    string `json:"full_name"`
	Avatar      string `json:"avatar"`
	PhoneNumber string `json:"phone_number"`
	Birthday    string `json:"birthday"`
	Password    string `json:"password"`
	Position    string `json:"position"`
}

type StandardErrorModel struct {
	Error string `json:"error"`
}