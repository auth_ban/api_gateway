package v1

import (
	"context"
	"net/http"

	"gitlab.com/auth_ban/api_gateway/internal/controller/http/models"
	"gitlab.com/auth_ban/api_gateway/internal/pkg/utils"

	//"gitlab.com/auth_ban/api_gateway/internal/service/register"
	"strings"

	"github.com/gin-gonic/gin"
	apb "gitlab.com/auth_ban/api_gateway/internal/genproto/auth_service"
	client "gitlab.com/auth_ban/api_gateway/internal/grpc_client"
)
type Controller struct {
	
}

func NewController() *Controller {
	return &Controller{}
}
// Login method user id va authorize qaytaradi
// @Description user id va authorize qaytaradi
// @Summary user id va authorize qaytaradi
// @Tags register
// @Accept json
// @Produce json
// @Param login body models.LoginRequest true "login"
// @Success 200 {object} models.LoginResponse
// @Failure 404 {object} models.StandardErrorModel
// @Failure 400 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /register/login/ [POST]
func (a *Controller) Login(c *gin.Context) {
	req := models.LoginRequest{}
	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if !strings.Contains(req.PhoneNumber, "+") || len(req.PhoneNumber) != 13 {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "phone number is not correctly filled",
		})
		return
	}

	res, err := client.AuthService().LogIn(context.Background(),&apb.LogInRequest{
		Password: req.Password,
		PhoneNumber: req.PhoneNumber,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	// Generate a new pair of access and refresh tokens.
	tokens, err := utils.GenerateNewTokens(res.UserID, map[string]string{
		"role": res.Role,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, models.LoginResponse{
		UserID:    res.UserID,
		Authorize: tokens.Access,
	})
}

// SignUp method sing up
// @Description sing up
// @Description avatar mazil yoziladi
// @Description birthday "2001-02-26" farmatda yoziladi
// @Summary sing up
// @Tags register
// @Accept json
// @Produce json
// @Param register body models.CreateUserRequest true "sing-up"
// @Success 200 {object} models.SuccessMessage
// @Failure 404 {object} models.StandardErrorModel
// @Failure 400 {object} models.StandardErrorModel
// @Failure 500 {object} models.StandardErrorModel
// @Router /register/sing-up/ [POST]
func (a *Controller) SignUp(c *gin.Context) {
	print("salom")
	req := models.CreateUserRequest{}
	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if !strings.Contains(req.PhoneNumber, "+") ||
		len(req.PhoneNumber) != 13 {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "phone number is not correctly filled",
		})
		return
	}

	_,err = client.AuthService().CreateUser(context.Background(),&apb.CreateUserRequest{
		FullName: req.FullName,
		Avatar: req.Avatar,
		Birthday: req.Birthday,
		Password: req.Password,
		PhoneNumber: req.PhoneNumber,
		Position: req.Position,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"result": models.SuccessMessage{
			Success: true,
		},
	})
}
