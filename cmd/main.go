package main

import (
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	_ "github.com/joho/godotenv/autoload" // load .env file automatically
	_ "gitlab.com/auth_ban/api_gateway/internal/controller/docs"
	task_controller "gitlab.com/auth_ban/api_gateway/internal/controller/http/v1"
	"gitlab.com/auth_ban/api_gateway/internal/middleware"
	"gitlab.com/auth_ban/api_gateway/internal/pkg/config"
	ginSwagger "github.com/swaggo/gin-swagger"
	swaggerFiles "github.com/swaggo/files"
)

var appConfig   = config.Config()

// @title API
// @version 1
// @description This is an auto-generated API Docs.
// @termsOfService http://swagger.io/terms/
// @contact.name Soburjon
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	taskController := task_controller.NewController()
	router := gin.Default()
		
	router.Use(cors.Default())
	jwtRoleAuthorizer, err := middleware.NewJWTRoleAuthorizer(appConfig)
	if err != nil {
		log.Fatal("Could not initialize JWT Role Authorizer")
	}
	router.Use(middleware.NewAuthorizer(jwtRoleAuthorizer))
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	router.POST("/register/sing-up/",taskController.SignUp)
	router.POST("/register/login/",taskController.Login)

		
	router.Run(":8000")
}
