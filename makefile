swag-gen:
	swag init -g cmd/main.go -o internal/controller/docs

proto-gen:
	./internal/scripts/gen-proto.sh  ${CURRENT_DIR}